/**
Day of the Sheep ship name generator by horusboyd.
@author horusboyd
@version 1.0 %G%
*/

import java.util.*;

public class NewNameGen implements NameGenerator {

	private ShipNameProvider provider;
	
	public NewNameGen( ShipNameProvider provider )
	{
		this.provider = provider;
	}
	
	/**
	Returns the name for a new ship. Adjective, object, prefix and template lists
	are pulled from the ShipNameProvider database. Elements of these lists are
	chosen at random and the name is created according to the chosen naming template.
	The name is compared with the previously used names, which are stored in the
	database, and a roman numeral is added at the end depending on if and how many
	times the name has been used before.

	@return the finished name.
	*/

	public String createName() {
		String name = "";
		String numeral = "";
		List<String> adjectivesList = provider.getStoredSegments("Adjective");
		List<String> objectsList = provider.getStoredSegments("Object");
		List<String> prefixList = provider.getStoredSegments("Prefix");
		List<String> templates = provider.getStoredSegments("Template");
		int indexToUse = rollDice(1, templates.size()) - 1;
		String templateToUse = templates.get(indexToUse);
		List<String> analyzedTemplate = analyze(templateToUse);
		name = buildCoreName(analyzedTemplate, adjectivesList, objectsList, prefixList);
		numeral = getShipNumeral(name);
		name = name + " " + numeral;		
		return name;
	}

	/**
	Rolls dice and returns the result. Used for picking random elements from lists.

	@param number the number of dice to be rolled.
	@param sides the number of sides for each dice to be rolled.
	@return the total of the rolled dice added together.
	*/

	private int rollDice(int number, int sides) {
		int total = 0;
		int roll = 0;
		Random r = new Random();
		for (int i = 0; i < number; i++) {
			roll = r.nextInt(sides) + 1;
			total = total + roll;
		}
		return total;
	}

	/**
	Takes a naming template string and converts it into a list, preserving the
	order of the elements. Based upon this list, a new name can be created.
	For example, the template string "AAO" will return a list containing two
	"Adjective" entries and one "Object" entry.

	@param template a naming template string.
	@return a list of elements required for creating a ship's name.
	*/

	private List<String> analyze(String template) {
		List<String> analyzedTemplate = new ArrayList<String>();
		for (int i = 0; i < template.length(); i++) {
			char ch = template.charAt(i);
			if (ch == 'A') {
				analyzedTemplate.add("Adjective");
			} else if (ch == 'O') {
				analyzedTemplate.add("Object");
			}
		}
		return analyzedTemplate;
	}

	/**
	Creates a new ship's name based upon a given naming template and given lists of adjectives, objects (nouns) and
	prefixes, without the roman numeral at the end. For each entry in the analyzed template list, a corresponding
	entry is picked at random from the lists of grammatical components and added to the name string. After each element,
	one of the prepositions (or none) is picked at random and added to the name.	Then, one of the articles (or none)
	is chosen at random and added to the name as well, in accordance with grammatical rules ("A" turns into "An" if the
	first letter of the name is a vowel). Finally, one of the prefixes is chosen at random and added at the beginning
	of the name.

	@param analyzedTemplate a list of elements indicating the required grammatical components of the name to be created.
	@param adjectivesList a list of adjectives that can be used during name creation.
	@param objectsList a list of objects (nouns) that can be used during name creation.
	@param prefixList a list of prefixes that can be used during name creation.
	@return the complete ship's name minus the possibly required roman numeral.
	*/

	private String buildCoreName(List<String> analyzedTemplate, List<String> adjectivesList, List<String> objectsList, List<String> prefixList) {
		String name = "";
		final String[] articles = {"", "A", "For", "The"};
		final String[] prepositions = {"", " of", " of the", " with"};

		for (int i = 0; i < analyzedTemplate.size(); i++) {
			if (analyzedTemplate.get(i).equals("Adjective")) {
				int adjIndex = rollDice(1, adjectivesList.size()) - 1;
				String adjToUse = adjectivesList.get(adjIndex);
				name = name + " " + adjToUse;
			} else if (analyzedTemplate.get(i).equals("Object")) {
				int objIndex = rollDice(1, objectsList.size()) - 1;
				String objToUse = objectsList.get(objIndex);
				name = name + " " + objToUse;
			}
			if ((i < (analyzedTemplate.size() - 1)) && (i > 0)) {
				int prepIndex = rollDice(1, prepositions.length) - 1;
				String prepToUse = prepositions[prepIndex];
				name = name + prepToUse;
			}
		}
		int articleIndex = rollDice(1, articles.length) - 1;
		String articleToUse = articles[articleIndex];
		char firstChar = name.charAt(1);
		if ((articleToUse.equals("A")) && ((firstChar == 'A') || (firstChar == 'E') || (firstChar == 'I') || (firstChar == 'O') || (firstChar == 'U'))) {
			name = " An" + name;
		} else if (articleToUse.equals("")) {
			name = articleToUse + name;
		} else {
			name = " " + articleToUse + name;
		}

		int prefixIndex = rollDice(1, prefixList.size()) - 1;
		String prefixToUse = prefixList.get(prefixIndex);
		name = prefixToUse + name;

		return name;
	}

	/**
	Converts a single roman numeral ("I", "V", "X", etc.) into an integer.

	@param numeral a roman numeral.
	@return the integer corresponding to the given numeral.
	*/

	private int getIntFromRoman(char numeral) {
		String roman = Character.toString(numeral);
		if (roman.equals("I")) {
			return 1;
		} else if (roman.equals("V")) {
			return 5;
		} else if (roman.equals("X")) {
			return 10;
		} else if (roman.equals("L")) {
			return 50;
		} else if (roman.equals("C")) {
			return 100;
		} else if (roman.equals("D")) {
			return 500;
		} else if (roman.equals("M")) {
			return 1000;
		} else {
			return 0;
		}
	}

	/**
	Converts a complex roman numeral (such as "II", "IX", "CDXX", etc.) into an integer.

	@param numeral a roman numeral.
	@return the integer corresponding to the given numeral.
	*/

	private int convertFromRoman(String numeral) {
		int result = 0;
		int carry = 0;
		int length = numeral.length();

		for (int i = 0; i < length - 1; i++) {
			if (getIntFromRoman(numeral.charAt(i)) > getIntFromRoman(numeral.charAt(i + 1))) {
				result = result + getIntFromRoman(numeral.charAt(i)) + carry;
				carry = 0;
			} else if (getIntFromRoman(numeral.charAt(i)) == getIntFromRoman(numeral.charAt(i + 1))) {
				carry = carry + getIntFromRoman(numeral.charAt(i));
			} else if (getIntFromRoman(numeral.charAt(i)) < getIntFromRoman(numeral.charAt(i + 1))) {
				carry = -carry - getIntFromRoman(numeral.charAt(i));
			}
		}
		result = result + carry + getIntFromRoman(numeral.charAt(length - 1));
		return result;
	}

	/**
	Converts an integer into a roman numeral.

	@param number an integer.
	@return the roman numeral corresponding to the given integer.
	*/

	private String convertToRoman(int number) {
		int n = number;
		String numeral = "";
		final String roman[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
		final int decimal[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

    	for (int i = 0; i < 13; i++) {
			while (n >= decimal[i]) {
				n = n - decimal[i];
				numeral = numeral + roman[i];
			}
		}
		return numeral;
	}

	/**
	Pulls the last used roman numeral for a given ship's name from the database and calculates
	and returns the	new numeral to be used. If the name is not found in the database, no numeral
	is required. If the name exists exactly once, the numeral "II" is used. If the name exists
	more than once, the last used numeral is converted to integer, 1 is added, and the result
	is converted into the new numeral to be used.

	@param coreName the complete ship's name without the roman numeral at the end.
	@return the numeral to be used at the end of the new ship's name.
	*/

	private String getShipNumeral(String coreName) {
		String newNumeral = "";
		
		List<String> existingNumbers = provider.getExistingShipNameNumbers(coreName);
		
		if ( existingNumbers == null ) // The shipName does not yet exist, so we don't need a numeral
			return newNumeral;
		else
		{
			int shipNumber = 1; // At least one ship with that base name exists so let's start with number 2 (1 because we will increment it)
			boolean exists = false;
			do
			{
				shipNumber++;
				newNumeral = convertToRoman(shipNumber);
				if ( stringListContains(existingNumbers, newNumeral))
					exists = true;
				else
					exists = false;
			}
			while ( exists );
		}
		
		return newNumeral;

	}
	
	private boolean stringListContains( List<String> haystack, String needle )
	{
		for ( String item : haystack )
		{
			if ( needle.equals(item))
				return true;
		}
		
		return false;
	}
}