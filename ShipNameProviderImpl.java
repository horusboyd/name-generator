//Fake ShipNameProvider for testing of the name generator.

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShipNameProviderImpl implements ShipNameProvider {

	private List<String> adjectives;
	private List<String> objects;
	private List<String> prefixes;
	private List<String> templates;
	
	private List<String> existingNames;
	
	public ShipNameProviderImpl()
	{
		fillDefaultValues();
		
		existingNames = new ArrayList<String>();
	}
	
	private void fillDefaultValues()
	{
		adjectives = new ArrayList<String>();
		adjectives.add("Innocent");
		adjectives.add("Smoky");
		adjectives.add("Beautiful");
		adjectives.add("Yellow");
		adjectives.add("Sick");
		adjectives.add("Orange");
		adjectives.add("Mushy");

		objects = new ArrayList<String>();
		objects.add("Mountain");
		objects.add("Water");
		objects.add("Shower");
		objects.add("Lettuce");
		objects.add("Apple");
		objects.add("Kettle");
		objects.add("Onion");
		objects.add("Mushroom");

		prefixes = new ArrayList<String>();
		prefixes.add("CS");
		prefixes.add("HMS");
		prefixes.add("IES");
		prefixes.add("SV");
		prefixes.add("USS");
		prefixes.add("FGS");

		templates = new ArrayList<String>();
		templates.add("O");
		templates.add("OO");
		templates.add("A");
		templates.add("AO");
		templates.add("AAO");
		templates.add("OOO");
		templates.add("OAO");
		templates.add("OAAO");
		templates.add("AOA");
		templates.add("AOO");
	}
	
	/** We need this here to fake our provider a bit
	 * 
	 * @param name
	 */
	public void addExistingName( String name )
	{
		existingNames.add(name);
	}

	public List<String> getExistingNames( String baseName ) {
		List<String> numbers = new ArrayList<String>();
		numbers.add("");
		return numbers;
	}

	public List<String> getStoredSegments( String key ) {
		

		if (key.equals("Adjective")) {
			return adjectives;
		} else if (key.equals("Object")) {
			return objects;
		} else if (key.equals("Prefix")) {
			return prefixes;
		} else if (key.equals("Template")) {
			return templates;
		} else {
			return null;
		}

	}

	@Override
	public List<String> getExistingShipNameNumbers(String baseName)
	{
		List<String> existingNumbers = null;
		
		// First we compile a regular expression pattern - thankfully some crazy guy already gave us the regular expression to check for Ship-Numbers :)
		Pattern p = Pattern.compile(ShipNameProvider.SHIP_NUMBER_REGEX); 
		
		// Let's go through all existing names to check if they have a number
		for ( String existingName : existingNames )
		{
			// Match the name against the regular expression
			Matcher m = p.matcher(existingName);
			
			if ( m.matches() )
			{
				if ( m.group(1).equals(baseName) ) // Oho - we actually have an existing name here which matches the given baseName
				{
					if ( existingNumbers == null )
						existingNumbers = new ArrayList<String>();
					
					if ( !m.group(2).equals("") )
						existingNumbers.add(m.group(2));
				}
			}
		}
		
		return existingNumbers;
	}

	@Override
	public Map<String, List<String>> getStoredSegmentsMap(String... keys)
	{
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		
		for ( String key : keys )
		{
			List<String> segment = getStoredSegments(key);
			if ( segment != null )
				result.put(key, segment);
		}
		
		return result;
	}
}