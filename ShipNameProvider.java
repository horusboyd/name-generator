

import java.util.List;
import java.util.Map;

public interface ShipNameProvider
{
	/**
	 * Regular expression to find out the numbering of a ship name
	 */
	public static final String SHIP_NUMBER_REGEX = "(.*?)[ ]?([XVI]*)$";
	
	/**
	 * Returns a list of already existing ship names similar to the given baseName.<br>
	 * 
	 * @param baseName
	 * @return
	 */
	public List<String> getExistingNames( String baseName );

	/** 
	 * Returns a list containing the roman numbers of the ships of the given baseName which already exist.<br>
	 * The function will return NULL if the baseName does not yet exist and an empty list if the baseName does exist only once.<br>
	 * Example:
	 * <ul>
	 * 	<li>"HMS Deathwish" and "HMS Deathwish II" already exist. Request for "HMS Deathwish" will return a list with one item {"II"}
	 * 	<li>"Captain's Dagger" already exists. Request for "Captain's Dagger" will return an empty list
	 * 	<li>"Second Chance" does not yet exist. Request for "Second Chance" will return NULL
	 * </ul>
	 * 
	 * @param baseName
	 * @return
	 */
	public List<String> getExistingShipNameNumbers( String baseName );
	
	
	/**
	 * Returns a list of stored Segments by a given key.<br>
	 * Example:<br>
	 * getStoredSegments("ShipPrefix") => {"CS", "HMS", "IES", "SV", "USS", "FGS",...}
	 * 
	 * @param key
	 * @return a list of stored Segments for the given key or NULL
	 * @see #getStoredSegmentsMap(String...)
	 */
	public List<String> getStoredSegments( String key );
	
	/**
	 * Returns a map of key=>segmentList by a varying number of given keys.<br>
	 * To optimize execution time this function should be used whenever you want to get more than one stored segment.
	 * 
	 * @param keys
	 * @return
	 * @see #getStoredSegments(String)
	 */
	public Map<String, List<String>> getStoredSegmentsMap( String... keys );
}
