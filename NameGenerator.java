public interface NameGenerator
{
 
	/** 
	 * Returns a generated name
	 * 
	 * @return
	 */
	public String createName();
}